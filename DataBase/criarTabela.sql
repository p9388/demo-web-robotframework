CREATE TABLE produtos_informatica_perifericos (
    id SERIAL PRIMARY KEY,
    nome VARCHAR(100),
    descricao TEXT,
    preco DECIMAL(10,2),
    data_cadastro DATE
);  