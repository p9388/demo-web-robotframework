*** Settings ***
Documentation  Fazer manipulaçoes com banco de dados

Library         psycopg2
Library         DatabaseLibrary

*** Variables ***

${DBHost}         localhost
${DBName}         produtos_informatica
${DBPass}         123456
${DBPort}         5432
${DBUser}         postgres

 

*** Keywords ***

Concetar 
     Connect To Database    psycopg2    ${DBName}    ${DBUser}    ${DBPass}    ${DBHost}    ${DBPort}

Desconectar
    Disconnect From Database

Executar Script de banco de dados
    [Arguments]     ${FILE}
    Execute SQL Script      sqlScriptFileName=${FILE}
    Execute SQL String    COMMIT
    
Verificar se ${PRODUTO} registro foi inserido
    Check If Exists In Database     selectStatement=SELECT nome from produtos_informatica WHERE nome= '${PRODUTO}'




    