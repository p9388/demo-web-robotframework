*** Settings ***
Documentation  Fazer manipulaçoes com banco de dados
Resource  ../Project_robot/Resources/db/db.robot

Library         DatabaseLibrary

Test Setup   Concetar
Test Teardown   Desconectar

*** Test Cases ***


Inserir produto script
     [Documentation]  Adicionar um registro valido e verificar que ele foi inserido
     [tags]  inserir
     Executar Script de banco de dados       ${CURDIR}/DataBase/inserirRegistro.sql
     Verificar se Mousepead registro foi inserido

Excluir produto via Script
      [Documentation]  Apagar um registro criado anteriomente 
      [tags]  excluir
     Executar Script de banco de dados       ${CURDIR}/DataBase/excluirRegistro.sql

